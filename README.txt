CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Ubercart Paypal Express Minimum order subtotal validation module make available for validating
minimum order subtotal for paypal express checkout on cart page.

INSTALLATION
------------------------------

* Download the module and copy it into your contributed modules folder:
 (for example, your_drupal_path/sites/all/modules)
  and enable it from the modules administration page
 http://drupal.org/node/1897420 for further information

CONFIGURATION:
---------------------------------

For configuration option you need to follow admin/store/settings/paypal-express-order-subtotal-validation.


MAINTAINERS
--------------------------------------------------------------------
 Current maintainer:
 * Vikas kumar - https://www.drupal.org/u/babusaheb.vikas