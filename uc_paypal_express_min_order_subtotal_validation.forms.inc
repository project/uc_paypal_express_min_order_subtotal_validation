<?php

/**
 * @file
 * Form definitions for forms defined in the Ubercart Paypal Express minimum order subtotal validation module.
 */

/**
 * Form definition for the Ubercart Paypal Express minimum order subtotal validation Settings Form.
 */
function uc_paypal_express_min_order_subtotal_validation_settings_form(array $form, array $form_state) {
  $form['apply_validation'] = [
    '#title' => t('Apply validation on order subtotal for Paypal Express checkout'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('uc_paypal_express_min_order_subtotal_validation_apply', FALSE),
    '#description' => t('If this box is checked, order subtotal validation will apply for Ubercart Paypal Express checkout payment method.'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Submission handler for the Ubercart Paypal Express minimum order subtotal validation Settings Form.
 */
function uc_paypal_express_min_order_subtotal_validation_settings_form_submit(array $form, array $form_state) {
  variable_set('uc_paypal_express_min_order_subtotal_validation_apply', $form_state['values']['apply_validation']);
  drupal_set_message('The settings have been updated');
}
